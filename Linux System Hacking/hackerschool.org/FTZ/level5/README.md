# level5
![ls -lR](ls.png)

When you open the hint file, it tells you 'The */usr/bin/level5* program creates a temporary file named *level5.tmp* in the */tmp* directory. Use this to gain *level6* authority.'

![cat hint](hint.png)

So I checked the permission of */usr/bin/level5*, which had SetUID of *level6*; and I checked whether there is a file named *level5.tmp* inside */tmp/* directory, and of course, there was nothing related to *level5.tmp*.

![/usr/bin/level5 and /tmp/level5.tmp](tmp.png)

If I execute */usr/bin/level5*, as it creates temporary file, I cannot see which file */usr/bin/level5* made. (I mean the content of */tmp/level5.tmp*.)

![execution of /usr/bin/level5](execute_level5.png)

Therefore, I artificially created */tmp/level5.tmp* file, and then excuted */usr/bin/level5*. As a result, I could get the password of *level6*, which is **what the hell**.

![level6 password](password.png)
