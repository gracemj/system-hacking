# level1
![ls -lR](ls.png)
When you open the hint file, it says, "Find the file with setuid on *level2* permission."
![cat hint](hint.png)
So, I tried to find *level2*'s file with `find` command.
`find` has a lot of options, and with good options, `find` becomes very strong. The options below are the ones I usually use for hacking.

 - `-name` : find with name
 - `-type` : find with type
 - `-perm` : find with permission
 - `-user` : find with owner (user)
 - `-atime` : find with accessed or opened time
 - `-mtime` : find with modified time
 - `-ctime` : find with time when its properties were modified
 - `-ls` : print with detailed information

[find --help](https://man7.org/linux/man-pages/man1/find.1.html)
I found a file called *ExecuteMe*:
![/bin/ExecuteMe](level2_setuid.png)
As it is an executable file with SetUID of *level2*, I executed it. And it allowed me to execute one command with the permission of *level2*, but without the following commands: `my-pass` and `chmod`. 
![execute /bin/ExecuteMe](ExecuteMe.png)
It was best to get a shell of *level2* right away because I had to figure out the password of *level2* or enter the *level2* stage. Since it was difficult to immediately determine the password for *level2* using `my-pass` command, the shell of *level2* was obtained through `/bin/bash`, and the password to level 2 was found by using the `my-pass` command in the given shell.
![/bin/bash and my-pass](bash_mypass.png)
So, the password for *level2* is **hacker or cracker**.
![su level2](su_level2.png)
