# level6
![hint](hint.png)

When I log in as *level6* which its password, as soon as I got an access, the hint was shown on the screen.

![bbs](bbs.png)

If I click on the *Enter* button, the program let me to type a number between 1 to 3; and if I type one of them, it tries to connect to a network.

![terminates the program](terminate.png)

I just wanted to escape from the program, so I just typed *Ctrl+C* for forceful termination of the program. And I could finally see what was on the home directory of *level6*.

![cat password](password.png)

There was password file inside the home directory of *level6*, and I could get the password simply.

![su level7](su_level7.png)

The password of *level7* is **come together**.
