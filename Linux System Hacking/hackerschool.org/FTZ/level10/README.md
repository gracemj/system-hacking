# level10
![ls -lR](ls.png)

There is a hint file inside the home directory of *level10*.

![hint](hint.png)

It says, "Two users are having a secret conversation using a chat room. The chat room was created using shared memory, and the value of *key_t* is 7530. Use this to eavesdrop on the conversation between the two and obtain the permission of *level11*.  - If you have completed this level, please erase the source code and leave."

![Wikipedia Shared Memory](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f2/Shared_memory.svg/600px-Shared_memory.svg.png)

> In [computer science](https://en.wikipedia.org/wiki/Computer_science "Computer science"), shared memory is [memory](https://en.wikipedia.org/wiki/Random-access_memory "Random-access memory") that may be simultaneously accessed by multiple programs with an intent to provide communication among them or avoid redundant copies. Shared memory is an efficient means of passing data between programs. Depending on context, programs may run on a single processor or on multiple separate processors. - Wikipedia

![ipcs -h](ipcs_help.png)

*ipcs* command reports interprocess communication facility status.

![ipcs -m](ipcs_m.png)

As the hint told me that the chat room is using shared memory, I checked the status of shared memory by using *m* option of *ipcs* command. The value of *key* is 0x00001d6a, which is 7530 in decimal; exactly the same value as the value of *key_t*.

![cat /proc/sysvipc/shm](proc_sysvipc_shm.png)

 - *key* = The key of the entry in hexadecimal
 - *cpid* = The process ID of the job that created the shared memory segment
 - *lpid* = The process ID of the last job to attach or detach from the shared memory segment or change the semaphore value

```c
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>

int main(){
	key_t key = 7530;
	int shmid = 0;
	char *str = (char*) shmat(shmid, (void*)0,0);
	printf("[*] Data read from memory: \n%s\n", str);
	shmdt(str);
	return 0;
}
```

I tried to attach myself to the shared memory segment using *shmat()*. `void *shmat(int shmid, void *shmaddr, int shmflg);` *shmid* is shared memory id. *shmaddr* specifies specific address to use but we should set it to zero and the system will automatically choose the address.

![password](password.png)

I compiled my source code and executed it. As a result, I could read the memory that the chat room was sharing, which is the password of *level11*. 

![su level11](su_level11.png)

The password of *level11* is **what!@#$?**.

