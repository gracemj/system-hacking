# level4
![ls -lR](ls.png)

When you open the hint file, it tells you 'Someone planted a backdoor in */etc/xinetd.d/*!'
![cat hint](hint.png)

So I looked through */etc/xinetd.d/* directory whether there is a backdoor inside in it or not. And I found a file named *backdoor*. As the group owner of */etc/xinetd.d/backdoor* is *level4* and I am the *level4*, I was allowed to READ the file.
![/etc/xinetd.d/](xinetd.png)

And the data below was inside the *backdoor* file:

    service finger
	{
		disable	= no
		flags		= REUSE
		socket_type	= stream
		wait		= no
		user		= level5
		server		= /home/level4/tmp/backdoor
		log_on_failure	+= USERID
	}
	
![/etc/xinetd.d/backdoor](backdoor.png)

So, what does */etc/xinetd.d/* directory include, and what is the service *finger*?

 - `/etc/xinetd.d/` : contains the configuration files for each service managed by xinetd and the names of the files correlate to the service.
 - `finger` : a user information lookup command which gives details of all the users logged in.

The configuration file of service *finger* shows me that the service executes */home/level4/tmp/backdoor* with the permission of *level5*. I had to use this.

When I looked inside */home/level4/tmp/* directory, there wasn't a file named *backdoor*. I planned to make a backdoor file that shows the password of *level5*, then executes the `finger` service.
![/home/level4/tmp/](tmp.png)

If I want the *backdoor* file to be executed, then I had to make it executable file. So, I made a short C-code file and compiled it.
![backdoor](compile_backdoor.png)

As I executes `finger` as below, I become *level5* temporarily and executes the backdoor file that I've created. I'm not really sure why the order of `printf()` and `system()` functions was different from what I've expected, but anyway, I've got the password of *level5*.
![finger](finger.png)

Now, I found the password of *level5*. It's **what is your name?**.
![su level5](su_level5.png)
