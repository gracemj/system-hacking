# level9
![ls -lR](ls.png)

There is a hint file inside the home directory of *level9*.

![hint](hint.png)

It shows the source code of */usr/bin/bof*. It says, "Use this to obtain the permission of *level10*". */usr/bin/bof* file looks like I have to do Buffer Overflow Attack in order to be *level10*.

![/usr/bin/bof](bof_bin.png)

As I expected, SetUID was set on the */usr/bin/bof* binary. So I couldn't use *gdb* on it. However, as I have the source code of */usr/bin/bof*, I will make my own *bof* binary.

![/tmp/test.c source code](bof_source.png)

*/usr/bin/bof* allows us to type 40 characters from the starting address of *buf*, even the actual *buf*'s allocated size is 10 characters. Then, it compares *buf2* with the string *"go"*; if it is the same, then it allows us to use */bin/bash* shell with the permission of *level10*; if not, it just ends the program. So, what I wanted to know was so simple, the starting address of *buf* and *buf2*. 

![check the address of buf and buf2](address.png)

When I executed the program, it let us know the difference between the starting address of *buf2* and *buf* is 0x10, which is 16 in decimal. From this result, I could expect that the program will give us the permission of *level10* if I type sixteen meaningless characters with the upcoming string "go".

![BoF on /usr/bin/bof](bof.png)

I typed sixteen "a"s and "go" when executing my *test* program. It is proved that the BoF that I expected actually works, so I did the same on */usr/bin/bof* program. As a result, it gave me the permission of *level10*.

![su level10](su_level10.png)

The password of *level10* is **interesting to hack!**.
