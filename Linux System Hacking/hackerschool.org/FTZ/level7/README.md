# level7
![ls -lR](ls.png)

There is a hint file inside the home directory of *level7*.

![hint](hint.png)

It says, "Executing the /bin/level7 command requests the password input."
"1. The password is located nearby."
"2. Use all your imagination."
"3. Can binary be converted into decimal?"
"4. Change the calculator settings to engineering mode."

![/bin/level7](level7.png)

When I execute */bin/level7*, as the hint told me, it allows me to enter the password. I don't know the password yet, so I entered the random string. And it shows a weird string. It seems like a binary, so I converted _ into 0, and - into 1.

![binary_first](1_1101101.png)

Then, the first binary will be 1101101. And if I convert this into decimal, it will be 109.

![binary_second](2_1100001.png)

The following three binaries will goes the same. The second one is 1100001 in binary, and 97 in decimal.

![binary_third](3_1110100.png)

Third one is 1110100 in binary, and 116 in decimal.

![binary_fourth](4_1100101.png)

Fourth one is 1100101 in binary, and 101 in decimal.

![integer to character](integer_to_character.png)
If I combine all of them, they will be 109, 97, 116, 101. And I tried to change them into character, because it made me think of ASCII. And yes, it became **mate** when I changed them into character.

![password](password.png)

I executed */bin/level7* again, and entered **mate** as the password. It told me the password of *level8* finally.

![su level8](su_level8.png)

The password of *level8* is **break the world**.
