# level2
![ls -lR](ls.png)
When you open the hint file, it says, "When editing a text file, I was told that I could execute the shell's command..."
![cat hint](hint.png)
So, AGAIN, I tried to find *level3*'s file with `find` command.
If you look at my *level1* commentary, you will see why I used `find` command with these options: `find -user "level3" -perm +4000 2>/dev/null`

I found a file called *editor* which has SetUID of *level3*, and this means that I am allowed to BORROW *level3*'s permission while I am accessing */usr/bin/editor*.
![/usr/bin/editor](level3_setuid.png)
As it is an executable file, I just executed it and VIM editor appeared. 
![execute /usr/bin/editor](vim.png)
The hint told me that it is able to execute the shell's command while editing, I used `:!COMMAND` in VIM command mode so that I could be like *level3*. Of course, I have to figure out *level3*'s password, I needed to use `my-pass` command in VIM command mode.
![execute my-pass](my-pass.png)
![level3's password](password.png)
Now, I found the password of *level3*. It's **can you fly?**.
![su level3](su_level3.png)
