# level3
![ls -lR](ls.png)
When you open the hint file, it shows the source code of *autodig*:

```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
    
int main(int argc, char **argv){
	char cmd[100];

	if( argc!=2 ){
	    printf( "Auto Digger Version 0.9\n" );
	    printf( "Usage : %s host\n", argv[0] );
	    exit(0);
	}

	strcpy( cmd, "dig @" );
	strcat( cmd, argv[1] );
	strcat( cmd, " version.bind chaos txt");

	system( cmd );	    
}
```

![cat hint](hint.png)
 - `strcpy` : copies the C string pointed by source into the array pointed by destination
 - `strcat` : appends a copy of the source string to the destination string

As the hint told me the source code of *autodig*, I tried to find the file named *autodig* with `find` command: `find -name "autodig" 2>/dev/null`
![ls -al autodig](autodig.png)
The file named *autodig* was located in */bin/* directory, and it had SetUID of *level4*, which means I can again borrow *level4*'s permission when executing */bin/autodig*. 

When I looked through the source code of *autodig*, that executable file requests a parameter when executing it. And, when a parameter was given, it makes a string as follows: `dig @PARAMETER version.bind chaos txt`
Then, it executes that string.

 - `dig` : (domain information groper) interrogates DNS name servers

When using `dig` command, target server should come after @. So, we can guess that the developer of */bin/autodig* would have wanted the user to input server information as the parameter. However, as */bin/autodig* has SetUID of *level4*, and I'm going to abuse it, I will insert `my-pass` command as its parameter because it's going to executes my input without any additional check.

Anyway, `dig` is also a command. In order to execute several commands in one line in Linux, I have to use `;` (semicolon) as shown below:
![semicolon](semicolon.png)

I will use this concept on */bin/autodig*. Of course, I can just ignore all the errors; however, I tried to have all successful results for every command. I intended */bin/autodig* to execute command as follows: `dig @8.8.8.8; my-pass; echo version.bind chaos txt`. Therefore, I only had to input "8.8.8.8; my-pass; echo" as its parameter.
![executed /bin/autodig](password.png)

Now, I found the password of *level4*. It's **suck my brain**.
![su level4](su_level4.png)
