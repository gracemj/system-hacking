# level8
![ls -lR](ls.png)

There is a hint file inside the home directory of *level8*.

![hint](hint.png)

It says, "The shadow file of *level9* is hidden somewhere on the server.  
All that is known about the file is that it has a capacity of "2700"."

![find command](find.png)

So, I tried to find a file sized 2700 by using *find* command. As a result, I could find a file: */etc/rc.d/found.txt*.

![found.txt](found.png)

When I opened */etc/rc.d/found.txt* file, I could see that it contains the hashed password of *level9*. 

![john the ripper](john.png)

I know that I can decrypt this shadow file using john the ripper. 

![su level9](su_level9.png)

The password of *level9* is **apple**.
