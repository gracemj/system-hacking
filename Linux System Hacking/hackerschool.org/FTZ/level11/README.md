# level11
![ls -lR](ls.png)

There is a hint file inside the home directory of *level11*.

![hint](hint.png)

The source code written in *hint* file seems to be the source code of the *attackme* binary. 

```c
#include <stdio.h>
#include <stdlib.h>

int main( int argc, char *argv[] )
{
	char str[256];

 	setreuid( 3092, 3092 );     // set real and effective user as level12
	strcpy( str, argv[1] );     // BoF can be occurred
	printf( str );
}
```

![id_level11](id_level12.png)

As the UID of *level11* is 3091, the UID of *level12* is probably 3092 (and it was.) Therefore, *attackme* binary gains the permission of *level12* by the function `setreuid`. It copies the first argument to *str* using `strcpy`, which has **buffer overflow vulnerability** as it does not specify the size of the destination array. 


