# FTZ (Free Training Zone)
## Getting started
First, you need to setup local environment on your PC.   
VMware, ssh clients and _VMware\_Redhat\_9\_FTZ.zip_ download is required.   
Once you downloaded, unzip the zip file and execute _Red Hat Linux.vmx_.   
Log in as a root user. The password for _root_ is _hackerschool_. That will be your challenge server.   
Then, check your server IP address with commands such as `ifconfig eth0` or `ip addr`.   
Execute your ssh client, and connect to your challenge server as user _level1_. If you are using terminal, just type `ssh level1@IP_ADDRESS`.   
The password for user _level1_ is _level1_. And you are ready to go!
### FYI
All hints are written in Korean. If you want to translate them by yourself, then you need additional setup.   
On your Red Hat server, change the character encoding to `LANG="ko_KR.euckr"` in _/etc/sysconfig/i18n_ file.   
If you are using iTerm as ssh client, then go to _Preference > Profiles > (Add new profile) > Terminal > Terminal Emulation > Character encoding_ and change it into _Korean (EUC)_.
## Challenge Sets
- [level1](level1)
- [level2](level2)
- [level3](level3)
- [level4](level4)
- [level5](level5)
- [level6](level6)
- [level7](level7)
- [level8](level8)
- [level9](level9)
- [level10](level10)
- [level11](level11)
