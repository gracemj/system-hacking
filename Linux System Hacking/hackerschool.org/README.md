# hackerschool.org
Here is my writeup for [FTZ challenges](https://www.hackerschool.org/Sub_Html/HS_FTZ/html/ftz_start.html) in [hackerschool.org](https://www.hackerschool.org/Sub_Html/HS_Community/index.html).
hackerschool.org is a hacking-related website that opened on Sep 9, 2000; and this was for Korean hackers or students.
## Challenge Sets
- [FTZ](FTZ) - stands for Free Training Zone
- [LOB](LOB) - stands for the Lord Of the Buffer overflow
